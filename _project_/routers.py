from rest_framework.routers import DefaultRouter

from core import views as main_views

router = DefaultRouter()

router.register(r'create-order', main_views.CrateOrdersViewSet)
router.register(r'orders', main_views.OrdersViewSet)
router.register(r'orders-edit', main_views.OrdersEditViewSet)
router.register(r'cities', main_views.CityViewSet)
router.register(r'sites', main_views.SiteViewSet)
router.register(r'drivers', main_views.DriverViewSet)