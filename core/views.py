import django_filters
import json

from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate
from django.conf import settings as django_settings
from django.views.decorators.csrf import csrf_exempt

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.authentication import BaseAuthentication, SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from rest_framework.decorators import permission_classes, api_view
from rest_framework.filters import SearchFilter
from django_filters import rest_framework as filters

from django.db.models import Count
from django.db.models import Q
from loguru import logger
from urllib.parse import unquote

from . import models as main_models
from . import serializers as main_serializers



# --------------- MANGO ---------------- #

# class MangoAuthentication(BaseAuthentication):
#     def authenticate(self, request):
#         if django_settings.MANGO_API_KEY in request.data.get('vpbx_api_key'):
#             user = main_models.Operator.objects.get(username='mango')
#             return (user, None)
#         else:
#             return None



@csrf_exempt
def mango_calls_api_view(request):
    try:
        mango_json = json.loads(unquote(str(request.body, encoding='utf-8')).split('&json=')[1])
        if mango_json.get('location') == 'ivr' and mango_json.get('call_state') == 'Appeared':
            site = main_models.Site.objects.get(phone=mango_json.get('to', {}).get('number'))
            client, cr_cl = main_models.Client.objects.get_or_create(phone='{}{}'.format('+', mango_json.get('from', {}).get('number')),
                                                                     defaults={'name': 'Не указано'})
            order, cr_or = main_models.Order.objects.get_or_create(site=site, client=client, defaults={'status': 8})

    except Exception as e:
        logger.add("file.log", format="{time} {level} {message}", level="INFO")
        mango_json = unquote(str(request.body, encoding='utf-8'))
        logger.info(f"error {e}")
        logger.info(f"{mango_json}")

    return HttpResponse(str(mango_json))


def get_atc_operator_id_by_client_phone(request):
    if request.GET.get('phone') and len(request.GET.get('phone')) > 9:
        # ищем заказы
        order = main_models.Order.objects.filter(client__phone__icontains=request.GET.get('phone')).select_related('operator', 'client')

        # ищем заказы где у операторов указан atc_id
        if order.filter(operator__atc_id__isnull=False):
            operator_id = order.filter(operator__atc_id__isnull=False).first().operator.atc_id
            data = {'operator_id': operator_id}
        # проверка существует ли заказ
        elif order.exists():
            data = {'error': 'Данный клиент не привязанн ни к одному из операторов'}
        # нет заказа с таким номером
        else:
            data = {'error': 'Нет клиента с текущим телефоном: {}'.format(request.GET.get('phone'))}
    elif request.GET.get('phone'):
        data = {'message': 'Номер должен содержать минимум 10 символов'}
    else:
        data = {'error': 'Не указан телефон'}

    return JsonResponse(data, json_dumps_params={'ensure_ascii': False})


class OrderFilter(filters.FilterSet):
    phone = django_filters.CharFilter(field_name="client__phone", lookup_expr="icontains")
    dir_from = django_filters.CharFilter(field_name="direction_from__name", lookup_expr="icontains")
    dir_to = django_filters.CharFilter(field_name="direction_to__name", lookup_expr="icontains")
    car = django_filters.CharFilter(field_name="auto", lookup_expr="icontains")
    dt_start = django_filters.DateTimeFilter(field_name="datetime", lookup_expr="gte")
    dt_end = django_filters.DateTimeFilter(field_name="datetime", lookup_expr="lte")

    class Meta:
        model = main_models.Order
        fields = ['status', 'phone', 'dir_from', 'dir_to', 'car', 'dt_start', 'dt_end']

class OrderPagination(PageNumberPagination):
    page_size = 25
    page_size_query_param = 'page_size'
    max_page_size = 100

    def get_paginated_response(self, data):
        per_page = self.page.paginator.per_page
        page = self.page.number
        count = self.page.paginator.count
        count_on_page = len(self.page.object_list)
        first = per_page * (page - 1) + 1
        last = per_page * (page - 1) + count_on_page
        num_pages = self.page.paginator.num_pages
        counts = main_models.Order.objects.select_related(
        'client', 'site', 'direction_from', 'direction_to', 'driver', 'operator').all().aggregate(
            new=Count('status', filter=Q(status=1)),
            from_call=Count('status', filter=Q(status=8)),
            price_calculate=Count('status', filter=Q(status=2)),
            contract=Count('status', filter=Q(status=3)),
            first_fifty=Count('status', filter=Q(status=4))
        )

        return Response({
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'num_pages': num_pages,
            'per_page': per_page,
            'page': page,
            'count_on_page': count_on_page,
            'count': count,
            'first': first,
            'last': last,
            'results': data,
            'counts': counts
        })

class CrateOrdersViewSet(viewsets.ModelViewSet):
    queryset = main_models.Order.objects.all()
    serializer_class = main_serializers.CreateOrderSerializer
    permission_classes = (IsAuthenticated,)


class OrdersEditViewSet(viewsets.ModelViewSet):
    queryset = main_models.Order.objects.all()
    serializer_class = main_serializers.OrderEditSerializer
    permission_classes = (IsAuthenticated,)


class OrdersViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = main_models.Order.objects.select_related(
        'client', 'site', 'direction_from', 'direction_to', 'driver', 'operator').all().order_by('-pk')
    serializer_class = main_serializers.OrderSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = OrderPagination
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = OrderFilter


class MiniResultsPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000


class CityFilter(filters.FilterSet):
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")

    class Meta:
        model = main_models.City
        fields = ['name']


class CityViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = main_models.City.objects.all()
    serializer_class = main_serializers.CitySerializer
    pagination_class = MiniResultsPagination
    permission_classes = (IsAuthenticated,)
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = CityFilter


class SiteFilter(filters.FilterSet):
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")

    class Meta:
        model = main_models.Site
        fields = ['name']


class SiteViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = main_models.Site.objects.all()
    serializer_class = main_serializers.SiteSerializer
    pagination_class = MiniResultsPagination
    permission_classes = (IsAuthenticated,)
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = SiteFilter


class DriverFilter(filters.FilterSet):
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")

    class Meta:
        model = main_models.Driver
        fields = ['name']


class DriverViewSet(viewsets.ModelViewSet):
    queryset = main_models.Driver.objects.all()
    serializer_class = main_serializers.DriverSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = MiniResultsPagination
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = DriverFilter