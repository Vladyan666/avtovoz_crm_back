from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
import re

class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            #raise ValueError('The given email must be set')
            email = ''
        email = self.normalize_email(email)
        user = self.model(username=username, email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, username, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, email, password, **extra_fields)


class Operator(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=200, unique=True, verbose_name='Имя пользователя')
    email = models.EmailField('Электронная почта', blank=True)
    phone = models.CharField(max_length=200, verbose_name='Телефон', null=True, blank=True)
    first_name = models.CharField('Имя', blank=True, max_length=100)
    last_name = models.CharField('Фамилия', blank=True, max_length=100)
    comment = models.TextField(verbose_name='Комментарий', null=True, blank=True)
    is_active = models.BooleanField('Активен?', default=False)
    is_staff = models.BooleanField('Статус персонала', default=False)
    is_superuser = models.BooleanField('Статус суперпользователя', default=False)
    date_joined = models.DateTimeField('Дата регистрации', auto_now_add=True)
    atc_id = models.PositiveSmallIntegerField('ATC ID', null=True, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return '%s' % (self.username, )

    class Meta:
        verbose_name = 'Оператор'
        verbose_name_plural = 'Оператор'


class City(models.Model):
    class Meta:
        verbose_name = u"Город"
        verbose_name_plural = u"Города"

    name = models.CharField(max_length=200, verbose_name=u"Название города")

    def __str__(self):
        return self.name


class Driver(models.Model):
    class Meta:
        verbose_name = u"Перевозчик"
        verbose_name_plural = u"Перевозчики"

    name = models.CharField(max_length=200, verbose_name=u"Имя перевозчика")
    phone = models.CharField(max_length=15, verbose_name=u"Телефон")
    info = models.TextField(verbose_name='Доп. информация', null=True, blank=True)
    comment = models.TextField(verbose_name='Комментарий', null=True, blank=True)

    def __str__(self):
        return self.name


class Source(models.Model):
    class Meta:
        verbose_name = u"Источник"
        verbose_name_plural = u"Источники"

    name = models.CharField(max_length=200, verbose_name=u"Название источника")

    def __str__(self):
        return self.name


class Site(models.Model):
    class Meta:
        verbose_name = u"Сайт"
        verbose_name_plural = u"Сайт"

    name = models.CharField(max_length=200, verbose_name=u"Название сайта")
    phone = models.CharField(max_length=200, verbose_name=u"Телефон", null=True, blank=True)

    def __str__(self):
        return self.name


class Client(models.Model):
    class Meta:
        verbose_name = u"Клиент"
        verbose_name_plural = u"Клиенты"

    name = models.CharField(max_length=200, verbose_name=u"Имя клиента", null=True, blank=True)
    phone = models.CharField(max_length=50, verbose_name=u"Телефон")
    info = models.TextField(verbose_name='Доп. информация', null=True, blank=True)
    comment = models.TextField(verbose_name='Комментарий', null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.phone = re.sub('\(|\)|-|\s', '', self.phone)
        super().save(*args, **kwargs)




METHOD = [
    (1, 'Наличные'),
    (2, 'Безнал'),
    (3, 'Безнал с НДС')
]

STATUS = [
    (0, 'Отказ'),
    (1, 'Новая заявка'),
    (2, 'Расчёт цены'),
    (3, 'Подписание договора'),
    (4, 'Первые 50%'),
    (5, 'Завершена'),
    (6, 'Перезвонить'),
    (7, 'Архив'),
    (8, 'Звонок с сайта')
]


class Order(models.Model):
    class Meta:
        verbose_name = u"Заявка"
        verbose_name_plural = u"Заявки"

    auto = models.CharField(max_length=200, verbose_name=u"Модель Авто", null=True, blank=True)
    number = models.CharField(max_length=200, verbose_name=u"Номер Договора", null=True, blank=True)
    order_price = models.FloatField(verbose_name='Цена перевозки', null=True, blank=True)
    driver_price = models.FloatField(verbose_name='Цена водителя', null=True, blank=True)
    delta = models.FloatField(verbose_name='Дельта', null=True, blank=True)
    datetime = models.DateTimeField(verbose_name=u"Время поступления заявки", auto_now_add=True)
    status = models.IntegerField(default=0, verbose_name='Статус заявки', choices=STATUS)
    direction_from = models.ForeignKey(City, related_name='from_city', on_delete=models.SET_NULL,
                                       verbose_name=u"Откуда", null=True, blank=True)
    direction_to = models.ForeignKey(City, related_name='to_city', on_delete=models.SET_NULL, verbose_name=u"Куда",
                                     null=True, blank=True)
    source = models.ForeignKey(Source, on_delete=models.CASCADE, verbose_name='Источник заявки', null=True, blank=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='Клиент')
    site = models.ForeignKey(Site, on_delete=models.CASCADE, verbose_name='Сайт')
    operator = models.ForeignKey(Operator, verbose_name=u"Логист", on_delete=models.SET_NULL, null=True, blank=True)
    driver = models.ForeignKey(Driver, verbose_name=u"Агент", on_delete=models.SET_NULL, null=True, blank=True)
    comment = models.TextField(verbose_name='Комментарий', null=True, blank=True)


    def save(self, *args, **kwargs):
        if self.order_price and self.driver_price:
            self.delta = self.order_price - self.driver_price
        else:
            self.delta = 0
        super().save(*args, **kwargs)


    def __str__(self):
        return str(self.pk)


class Payment(models.Model):
    class Meta:
        verbose_name = u"Плтёж"
        verbose_name_plural = u"Платежи"

    pay_id = models.CharField(max_length=200, verbose_name=u"Id платежа")
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="payments", verbose_name="Оплата")
    method = models.CharField(max_length=200, choices=METHOD, verbose_name='Метод оплаты')
    sum = models.FloatField(verbose_name='Сумма платежа')
    pay_date = models.DateTimeField(verbose_name=u"Время платежа", auto_now_add=True)

    def __str__(self):
        return self.pay_id
