from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Client)
admin.site.register(Order)
admin.site.register(Driver)
admin.site.register(Operator)
admin.site.register(Site)
class CityAdmin(admin.ModelAdmin):
    search_fields = ['name']

admin.site.register(City, CityAdmin)