from django.core.exceptions import MultipleObjectsReturned
from rest_framework import serializers
from . import models as main_models


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = main_models.Client
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):

    site = serializers.SlugRelatedField(read_only=True, slug_field='name')
    status_name = serializers.CharField(source='get_status_display')
    direction_from = serializers.SerializerMethodField(read_only=True)
    direction_to = serializers.SerializerMethodField(read_only=True)
    from_to = serializers.SerializerMethodField(read_only=True)
    driver = serializers.SerializerMethodField(read_only=True)
    site_name = serializers.SerializerMethodField(read_only=True)
    source = serializers.SlugRelatedField(read_only=True, slug_field='name')
    client = ClientSerializer()
    status = serializers.SerializerMethodField(read_only=True)

    def get_from_to(self, obj):
        if obj.direction_from and obj.direction_to:
            return f'{obj.direction_from.name} --> {obj.direction_to.name}'
        else:
            return 'Не указано'


    def get_status(self, obj):
        return {'label': obj.get_status_display(), 'value': obj.status} if obj.status else None

    def get_direction_from(self, obj):
        return {'label': obj.direction_from.name, 'value': obj.direction_from.id} if obj.direction_from else None

    def get_direction_to(self, obj):
        return {'label': obj.direction_to.name, 'value': obj.direction_to.id} if obj.direction_to else None

    def get_driver(self, obj):
        return {'label': obj.driver.name, 'value': obj.driver.id} if obj.driver else None

    def get_driver(self, obj):
        return {'label': f'{obj.driver.name} {obj.driver.phone}', 'value': obj.driver.id} if obj.driver else None

    def get_site(self, obj):
        return {'label': obj.site.name, 'value': obj.site.id} if obj.site else None

    def get_site_name(self, obj):
        return obj.site.name


    class Meta:
        model = main_models.Order
        fields = ('id', 'auto', 'status_name', 'from_to', 'site', 'number', 'order_price', 'driver_price', 'datetime', 'status',
                  'direction_from', 'direction_to', 'source', 'delta', 'client', 'site_name', 'operator', 'driver', 'comment')


class OrderEditSerializer(serializers.ModelSerializer):

    client = ClientSerializer()

    class Meta:
        model = main_models.Order
        fields = '__all__'

    def update(self, instance, validated_data):
        client = validated_data.pop('client', None)
#        order = main_models.Order.objects.filter(pk=order_id).update(*validated_data)
        for key, value in validated_data.items():
            setattr(instance, key, value)

        for key, value in client.items():
            setattr(instance.client, key, value)
        instance.client.save()
        instance.save()

        return instance



class CreateOrderSerializer(serializers.ModelSerializer):

    site_name = serializers.CharField(label='Название сайта', required=True, write_only=True)
    client_name = serializers.CharField(max_length=200, label='Имя клиента', default='Не указано',
                                        required=False, write_only=True)
    client_phone = serializers.CharField(max_length=200, label='Телефон клиента', required=True, write_only=True)
    direction_to = serializers.CharField(label='Город отправления', required=False, write_only=True)
    direction_from = serializers.CharField(label='Город прибытия', required=False, write_only=True)

    class Meta:
        model = main_models.Order
        fields = ('pk', 'site_name', 'client_name', 'client_phone', 'direction_to', 'direction_from',
                  'auto', 'order_price', 'driver_price', 'number', 'status')


    def _get_city(self, name):
        if name:
            if main_models.City.objects.filter(name=name).exists():
                return main_models.City.objects.get(name=name)
            else:
                return main_models.City.objects.create(name=name.capitalize())
        else:
            return None


    def create(self, validated_data):
        validated_data['direction_to'] = self._get_city(validated_data.get('direction_to'))
        validated_data['direction_from'] = self._get_city(validated_data.get('direction_from'))
        client = main_models.Client.objects.get_or_create(phone=validated_data.pop('client_phone'),
                                                          defaults={'name': validated_data.pop('client_name', '')})[0]
        site = main_models.Site.objects.get_or_create(name=validated_data.pop('site_name'))[0]
        order = main_models.Order.objects.create(site=site, client=client, **validated_data)

        return order


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = main_models.City
        fields = ('pk', 'name')


class SiteSerializer(serializers.ModelSerializer):

    class Meta:
        model = main_models.Site
        fields = ('pk', 'name')


class DriverSerializer(serializers.ModelSerializer):

    class Meta:
        model = main_models.Driver
        fields = '__all__'